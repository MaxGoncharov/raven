﻿namespace Raven.Core
{
	public class TextMessage : Message
	{
		public string Text { get; set; }
	}
}