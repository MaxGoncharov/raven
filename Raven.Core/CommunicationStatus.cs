﻿namespace Raven.Core
{
	public enum CommunicationStatus
	{
		Disconnected,
		Connecting,
		Connected,
		Error,
	}
}