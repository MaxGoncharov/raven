﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Raven.Core
{
	public class Crypto : IDisposable
	{
		private readonly byte[] _salt = { 17, 11, 13, 2, 7, 19, 5, 3 };
		private readonly RijndaelManaged _aesAlg = new RijndaelManaged();
		private readonly ICryptoTransform _encryptor;
		private readonly ICryptoTransform _decryptor;

		private static Encoding DefaultEncoding => Encoding.UTF8;

		public Crypto(string password)
		{
			if (string.IsNullOrEmpty(password))
			{
				throw new ArgumentNullException(nameof(password));
			}
			
			var key = new Rfc2898DeriveBytes(password, _salt);
			_aesAlg.Padding = PaddingMode.Zeros;
			_aesAlg.Key = key.GetBytes(_aesAlg.KeySize / 8);
			_aesAlg.IV = new byte[] { 97, 12, 67, 21, 43, 17, 57, 35, 79, 67, 27, 113, 59, 73, 89, 83 };
			_encryptor = _aesAlg.CreateEncryptor(_aesAlg.Key, _aesAlg.IV);
			_decryptor = _aesAlg.CreateDecryptor(_aesAlg.Key, _aesAlg.IV);
		}

		public byte[] Encrypt(string text)
		{
			text = text ?? string.Empty;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (CryptoStream cryptoStream = new CryptoStream(memoryStream, _encryptor, CryptoStreamMode.Write))
				{
					using (StreamWriter writer = new StreamWriter(cryptoStream, DefaultEncoding))
					{
						writer.Write(text);
						writer.Flush();
						cryptoStream.FlushFinalBlock();

						return memoryStream.ToArray();
					}
				}
			}
		}

		public string Decrypt(byte[] bytes)
		{
			using (MemoryStream memoryStream = new MemoryStream(bytes))
			{
				using (CryptoStream cryptoStream = new CryptoStream(memoryStream, _decryptor, CryptoStreamMode.Read))
				{
					using (StreamReader reader = new StreamReader(cryptoStream, DefaultEncoding))
					{
						return reader.ReadToEnd();
					}
				}
			}
		}

		public void Dispose()
		{
			_decryptor.Dispose();
			_encryptor.Dispose();
			_aesAlg.Dispose();
		}
	}
}