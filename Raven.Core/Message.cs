﻿using System;
using System.Collections.Generic;

namespace Raven.Core
{
	public abstract class Message
	{
		public long Number { get; set; }
		public string FromGuid { get; set; }
		public string ToGuid { get; set; }
		public DateTimeOffset Timestamp { get; set; } = DateTimeOffset.Now;
		public Dictionary<string, string> Extensions { get; set; }
	}
}
