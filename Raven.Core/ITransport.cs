﻿using System;
using System.Threading.Tasks;

namespace Raven.Core
{
	public delegate void ReciveTextMessage(ITransport transport, TextMessage message);

	public delegate void ClientConnected(ITransport transport, Client client);

	public delegate void ChangedStatusTransport(ITransport transport, string guid, CommunicationStatus status);

	public class Client
	{
		public string Guid { get; set; }

		public string DisplayName { get; set; }

		public string ConnectionString { get; set; }

		public override bool Equals(object obj)
		{
			var other = obj as Client;
			if (other == null)
			{
				return false;
			}
			return other.Guid == Guid;
		}

		public override int GetHashCode() => Guid?.Length ?? 0;
	}

	public interface ITransport : IDisposable
	{
		string Guid { get; }

		string ConnectionString { get; }

		void SetMyTransportSettings(Client client);

		void Initialization();

		Task Start(Client self);
		void Stop();

		void AddEndpoint(params string[] connectionStrings);
		void RemoveEndpoint(params string[] connectionStrings);
		string[] GetEndpoints();
		
		Task<bool> SendTextMessage(TextMessage message);

		event ReciveTextMessage ReciveTextMessageEvent;
		event ChangedStatusTransport ChangedStatusTransportEvent;
		event ClientConnected ClientConnectedEvent;
	}
}