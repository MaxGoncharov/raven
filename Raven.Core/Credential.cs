﻿namespace Raven.Core
{
	public class Credential
	{
		public Credential() { }

		public Credential(string username, string password)
		{
			Username = username;
			Password = password;
		}

		public string Username { get; set; }

		public string Password { get; set; }
	}
}
