﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Raven.Pages.Work;

namespace Raven.Converters
{
	public class MessagePositionConverter : MarkupExtension, IValueConverter
	{
		public const string Right = "R";
		public const string Left = "L";

		private static MessagePositionConverter _converter;
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var mess = (TextMessageViewModel)value;

			bool res = mess?.User == null;
			if (parameter?.ToString() == Right)
			{
				res = !res;
			}

			return res ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return _converter ?? (_converter = new MessagePositionConverter());
		}
	}

}
