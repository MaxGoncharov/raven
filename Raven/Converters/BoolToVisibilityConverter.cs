﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Raven.Converters
{
	public class BoolToVisibilityConverter : MarkupExtension, IValueConverter
	{
		private static BoolToVisibilityConverter _converter;

		public const string InvertToken = "invert";
		public const string HiddenToken = "hidden";

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var b = value != null && (bool) value;
			if (parameter?.ToString().ToLowerInvariant().Trim() == InvertToken)
			{
				b = !b;
			}
			var unvisible = Visibility.Collapsed;
			if (parameter?.ToString().ToLowerInvariant().Trim() == HiddenToken)
			{
				unvisible = Visibility.Hidden;
			}

			return b ? Visibility.Visible : unvisible;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return _converter ?? (_converter = new BoolToVisibilityConverter());
		}
	}
}
