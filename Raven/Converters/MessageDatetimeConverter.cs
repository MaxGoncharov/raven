﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace Raven.Converters
{
	public class MessageDatetimeConverter : MarkupExtension, IValueConverter
	{
		private static MessageDatetimeConverter _converter;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var date = (DateTimeOffset?) value ?? DateTimeOffset.MinValue;
			if (date.Date == DateTimeOffset.Now.Date)
			{
				return date.LocalDateTime.ToString("HH:mm");
			}
			return date.LocalDateTime.ToString("HH:mm dd/mm/yyyy");
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return _converter ?? (_converter = new MessageDatetimeConverter());
		}
	}
}