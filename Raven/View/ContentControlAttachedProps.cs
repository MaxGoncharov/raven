﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Raven.View
{
	public static class ContentControlAttachedProps
	{
		public static readonly DependencyProperty ContentChangedAnimationProperty = DependencyProperty.RegisterAttached(
			"ContentChangedAnimation", typeof(Storyboard), typeof(ContentControlAttachedProps), new PropertyMetadata(default(Storyboard), ContentChangedAnimationPropertyChangedCallback));

		public static void SetContentChangedAnimation(DependencyObject element, Storyboard value)
		{
			element.SetValue(ContentChangedAnimationProperty, value);
		}

		public static Storyboard GetContentChangedAnimation(DependencyObject element)
		{
			return (Storyboard)element.GetValue(ContentChangedAnimationProperty);
		}

		private static void ContentChangedAnimationPropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			var contentControl = (ContentControl)obj;
			var propertyDescriptor = DependencyPropertyDescriptor.FromProperty(ContentControl.ContentProperty, typeof(ContentControl));
			propertyDescriptor.RemoveValueChanged(contentControl, ContentChangedHandler);
			propertyDescriptor.AddValueChanged(contentControl, ContentChangedHandler);
		}

		private static void ContentChangedHandler(object sender, EventArgs eventArgs)
		{
			var animateObject = (FrameworkElement)sender;
			var storyboard = GetContentChangedAnimation(animateObject);
			storyboard.Begin(animateObject);
		}
	}
}