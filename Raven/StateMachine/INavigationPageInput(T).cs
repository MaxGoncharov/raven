﻿using System.Threading.Tasks;

namespace Raven.StateMachine
{
	public interface INavigationPageInput<TInput>
	{
		Task Initialization(TInput input);
	}
}