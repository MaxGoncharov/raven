using System.Threading.Tasks;

namespace Raven.StateMachine
{
	public interface INavigationPageInput
	{
		Task Initialization();
	}
}