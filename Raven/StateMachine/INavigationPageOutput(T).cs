using System;

namespace Raven.StateMachine
{
	public interface INavigationPageOutput<TTokenDirection>
		where TTokenDirection : IComparable
	{
		event NavigatePageCallback<TTokenDirection> NavigatePageEvent;
	}
}