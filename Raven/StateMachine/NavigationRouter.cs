using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Raven.Annotations;

namespace Raven.StateMachine
{
	public abstract class NavigationRouter : INotifyPropertyChanged, IDisposable
	{
		private readonly List<Action> _subsrubes = new List<Action>();
		private readonly List<Out> _outs = new List<Out>();
		private readonly List<Link> _relations = new List<Link>();
		private readonly List<Exit> _exits = new List<Exit>();
		private readonly Dictionary<Type, object> _instances = new Dictionary<Type, object>();
		private object _content;

		public object Content
		{
			get { return _content; }
			private set
			{
				if (Equals(value, _content)) return;
				_content = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public virtual void Dispose()
		{
			ClearSubscriptions();
			var content = Content;
			Content = null;
			(content as IDisposable)?.Dispose();
			_outs.Clear();
			_relations.Clear();
			_exits.Clear();
			_instances.Clear();
		}

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		protected void RegistryStaticInstance<T>(T instance)
			where T : class
		{
			_instances[typeof(T)] = instance;
		}

		protected async Task Run<TStart, TInput>(TInput input)
			where TStart : class, INavigationPageInput<TInput>
		{
			var start = GetInstance<TStart>();
			foreach (var el in _outs.Where(i => i.From == typeof(TStart)))
			{
				_subsrubes.Add(el.Subsrube(start));
			}
			Content = start;
			await start.Initialization(input);
		}

		protected async Task Run<TStart>()
			where  TStart : class, INavigationPageInput
		{
			var start = GetInstance<TStart>();
			foreach (var el in _outs.Where(i => i.From == typeof(TStart)))
			{
				_subsrubes.Add(el.Subsrube(start));
			}
			Content = start;
			await start.Initialization();
		}

		protected void Bind<TFrom, TTo, TTokenDirection>(TTokenDirection tokenDirection)
			where TFrom : INavigationPageOutput<TTokenDirection>
			where TTo : class, INavigationPageInput
			where TTokenDirection : IComparable
		{
			if (_outs.All(q => !q.From.IsAssignableFrom(typeof(INavigationPageOutput<TTokenDirection>))))
			{
				RegistryOut<TFrom, TTokenDirection>();
			}

			var item = new Link(typeof(TFrom), tokenDirection);
			item.Moving = async res =>
			{
				ClearSubscriptions();
				var to = GetInstance<TTo>();
				foreach (var el in _outs.Where(i => i.From == typeof(TTo)))
				{
					_subsrubes.Add(el.Subsrube(to));
				}
				Content = to;
				await to.Initialization();
			};

			_relations.Add(item);
		}

		protected void Bind<TFrom, TTo, TTokenDirection, TResult>(TTokenDirection tokenDirection)
			where TFrom : INavigationPageResultOutput<TTokenDirection, TResult>
			where TTo : class, INavigationPageInput<TResult>
			where TTokenDirection : IComparable
		{
			if (_outs.All(q => !q.From.IsAssignableFrom(typeof(INavigationPageResultOutput<TTokenDirection, TResult>))))
			{
				RegistryOut<TFrom, TTokenDirection, TResult>();
			}

			var item = new Link(typeof(TFrom), tokenDirection);
			item.Moving = async res =>
			{
				ClearSubscriptions();
				var to = GetInstance<TTo>();
				foreach (var el in _outs.Where(i => i.From == typeof(TTo)))
				{
					_subsrubes.Add(el.Subsrube(to));
				}
				Content = to;
				await to.Initialization((TResult)res);
			};

			_relations.Add(item);
		}

		protected void End<TFrom, TTokenDirection>(TTokenDirection tokenDirection, Action action)
			where TFrom : INavigationPageOutput<TTokenDirection>
			where TTokenDirection : IComparable
		{
			if (_outs.All(q => !q.From.IsAssignableFrom(typeof(INavigationPageOutput<TTokenDirection>))))
			{
				RegistryOut<TFrom, TTokenDirection>();
			}

			ClearSubscriptions();
			_exits.Add(new Exit(typeof(TFrom), tokenDirection, action));
		}

		private void RegistryOut<TFrom, TTokenDirection>()
			where TFrom : INavigationPageOutput<TTokenDirection>
			where TTokenDirection : IComparable
		{
			Func<object, Action> subsrube = obj =>
			{
				var from = (TFrom)obj;

				NavigatePageCallback<TTokenDirection> callback = direction =>
				{
					var exit = _exits.FirstOrDefault(i => i.From == typeof(TFrom) && Equals(i.TokenDirection, direction));
					if (exit != null)
					{
						exit.Action?.Invoke();
					}
					else
					{
						var next = _relations.FirstOrDefault(i => i.From == typeof(TFrom) && Equals(i.TokenDirection, direction));
						if (next != null)
						{
							next.Moving(null);
						}
						else
						{
							Debug.Assert(false);
						}
					}
					DisposeObject(from);
				};
				from.NavigatePageEvent += callback;
				return () => from.NavigatePageEvent -= callback;
			};
			_outs.Add(new Out(typeof(TFrom), subsrube));
		}

		private void RegistryOut<TFrom, TTokenDirection, TResult>()
			where TFrom : INavigationPageResultOutput<TTokenDirection, TResult>
			where TTokenDirection : IComparable
		{
			Func<object, Action> subsrube = obj =>
			{
				var from = (TFrom)obj;
				NavigatePageResultCallback<TTokenDirection, TResult> callback = (direction, result) =>
				{
					var exit = _exits.FirstOrDefault(i => i.From == typeof(TFrom) && Equals(i.TokenDirection, direction));
					if (exit != null)
					{
						exit.Action?.Invoke();
					}
					else
					{
						var next = _relations.FirstOrDefault(i => i.From == typeof(TFrom) && Equals(i.TokenDirection, direction));
						if (next != null)
						{
							next.Moving(result);
						}
						else
						{
							Debug.Assert(false);
						}
					}
					DisposeObject(from);
				};
				from.NavigatePageResultEvent += callback;
				return () => from.NavigatePageResultEvent -= callback;
			};
			_outs.Add(new Out(typeof(TFrom), subsrube));
		}

		private void ClearSubscriptions()
		{
			foreach (var action in _subsrubes)
			{
				action();
			}
			_subsrubes.Clear();
		}

		private T GetInstance<T>()
			where T : class
		{
			var type = typeof(T);
			if (_instances.ContainsKey(type))
			{
				return _instances[type] as T;
			}
			return Activator.CreateInstance<T>();
		}

		private void DisposeObject(object obj)
		{
			if (!_instances.ContainsKey(obj.GetType()))
			{
				(obj as IDisposable)?.Dispose();
			}
		}

		private class Link
		{
			public Link(Type from, IComparable tokenDirection)
			{
				From = from;
				TokenDirection = tokenDirection;
			}
			public Type From { get; }
			public IComparable TokenDirection { get; }

			public Action<object> Moving { get; set; }
		}

		private class Exit
		{
			public Exit(Type from, IComparable tokenDirection, Action action)
			{
				From = from;
				TokenDirection = tokenDirection;
				Action = action;
			}
			public Type From { get; }
			public IComparable TokenDirection { get; }

			public Action Action { get; }
		}

		private class Out
		{
			public Out(Type from, Func<object, Action> subsrube)
			{
				From = from;
				Subsrube = subsrube;
			}

			public Type From { get; }
			
			public Func<object, Action> Subsrube { get; }
		}
	}
}