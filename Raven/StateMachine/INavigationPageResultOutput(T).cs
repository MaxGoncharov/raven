using System;

namespace Raven.StateMachine
{
	public interface INavigationPageResultOutput<TTokenDirection, TResult>
		where TTokenDirection : IComparable
	{
		event NavigatePageResultCallback<TTokenDirection, TResult> NavigatePageResultEvent;
	}
}