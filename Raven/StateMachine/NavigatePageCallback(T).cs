namespace Raven.StateMachine
{
	public delegate void NavigatePageCallback<TTokenDirection>(TTokenDirection direction);
}