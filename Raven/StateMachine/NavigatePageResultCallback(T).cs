namespace Raven.StateMachine
{
	public delegate void NavigatePageResultCallback<TTokenDirection, TResult>(TTokenDirection direction, TResult result);
}