﻿using System;
using System.Threading.Tasks;

namespace Raven.Commands
{
	public class AsyncCommand : AsyncCommand<object>
	{
		public AsyncCommand(Func<Task> execute) : base(o => execute())
		{
		}

		public AsyncCommand(Func<Task> execute, Func<bool> canExecute) : base(o => execute(), o => canExecute())
		{
		}
	}
}