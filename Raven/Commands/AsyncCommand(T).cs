﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Raven.Annotations;

namespace Raven.Commands
{
	public class AsyncCommand<TParam> : ICommand, INotifyPropertyChanged
	{
		private CancellationTokenSource _cancellationTokenSource;
		private readonly Func<TParam, CancellationToken, Task> _execute;
		private readonly Func<TParam, bool> _canExecute;
		private bool _isExecuting;

		public bool IsExecuting
		{
			get { return _isExecuting; }
			private set
			{
				if (value == _isExecuting) return;
				_isExecuting = value;
				OnPropertyChanged();
			}
		}

		public void Cancel()
		{
			_cancellationTokenSource?.Cancel();
			_cancellationTokenSource?.Dispose();
		}

		public AsyncCommand(Func<TParam, Task> execute) : this(execute, param => true)
		{
		}

		public AsyncCommand(Func<TParam, Task> execute, Func<TParam, bool> canExecute) : this((p, c) => execute(p), canExecute)
		{
		}

		public AsyncCommand(Func<TParam, CancellationToken, Task> execute, Func<TParam, bool> canExecute)
		{
			_execute = execute;
			_canExecute = canExecute;
		}

		public bool CanExecute(object parameter)
		{
			return !IsExecuting && _canExecute((TParam)parameter);
		}

		public event EventHandler CanExecuteChanged;

		public async void Execute(object parameter)
		{
			_cancellationTokenSource = new CancellationTokenSource();
			IsExecuting = true;
			OnCanExecuteChanged();
			try
			{
				await _execute((TParam)parameter, _cancellationTokenSource.Token);
			}
			finally
			{
				IsExecuting = false;
				OnCanExecuteChanged();
			}
		}

		protected virtual void OnCanExecuteChanged()
		{
			CanExecuteChanged?.Invoke(this, new EventArgs());
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}