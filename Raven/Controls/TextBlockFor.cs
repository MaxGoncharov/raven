﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Raven.Controls
{
	public class TextBlockFor : TextBlock
	{
		private DisplayNameAttribute _displayAttribute;
		private TextSourceMode _sourceMode = TextSourceMode.Unknow;

		public static readonly DependencyProperty TextForProperty = DependencyProperty.Register("TextFor", typeof(string), typeof(TextBlockFor));
		public static readonly DependencyProperty TextFormatProperty = DependencyProperty.Register("TextFormat", typeof(CaptionFormat), typeof(TextBlockFor));
		public static readonly DependencyProperty TextCustomProperty = DependencyProperty.Register("TextCustom", typeof(string), typeof(TextBlockFor));

		public TextBlockFor()
		{
			Loaded += OnLoaded;
		}

		public CaptionFormat TextFormat
		{
			get { return (CaptionFormat)GetValue(TextFormatProperty); }
			set { SetValue(TextFormatProperty, value); }
		}

		public string TextFor
		{
			get { return (string)GetValue(TextForProperty); }
			set { SetValue(TextForProperty, value); }
		}

		public string TextCustom
		{
			get { return (string)GetValue(TextCustomProperty); }
			set { SetValue(TextCustomProperty, value); }
		}

		private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			Loaded -= OnLoaded;
			if (!string.IsNullOrEmpty(Text))
			{
				_sourceMode = TextSourceMode.Unknow;
				return;
			}

			_sourceMode = !string.IsNullOrEmpty(TextCustom) ? TextSourceMode.Custom : TextSourceMode.FromControl;
			if (_sourceMode == TextSourceMode.FromControl)
			{
				var binding = BindingOperations.GetBindingExpression(this, TextForProperty);
				if (binding == null)
				{
					_sourceMode = TextSourceMode.Unknow;
					return;
				}

				var propertyName = binding.ParentBinding != null ? binding.ParentBinding.Path.Path : binding.ResolvedSourcePropertyName;
				var data = binding.DataItem ?? DataContext;

				var type = data?.GetType();
				var list = propertyName.Split('.').ToArray();
				for (int i = 0; i < list.Length - 1; ++i)
				{
					var prop = type?.GetProperty(list[i]);
					type = prop?.PropertyType;
				}

				var bindedProperty = type?.GetProperty(list.Last());

				_displayAttribute = (DisplayNameAttribute)bindedProperty?
					.GetCustomAttributes(typeof(DisplayNameAttribute), false)
					.FirstOrDefault();
				if (_displayAttribute == null)
				{
					_sourceMode = TextSourceMode.Unknow;
				}
			}

			UpdateText();
		}

		private void UpdateText()
		{
			switch (_sourceMode)
			{
				case TextSourceMode.Custom:
					Text = GetFormattedText(TextCustom);
					break;
				case TextSourceMode.FromControl:
					Text = GetFormattedText(_displayAttribute.DisplayName);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private string GetFormattedText(string inputText)
		{
			switch (TextFormat)
			{
				case CaptionFormat.None: 
					return inputText;
				case CaptionFormat.FieldName:
					return $"{inputText}:";
				default:
					return string.Empty;
			}
		}

		private enum TextSourceMode
		{
			Unknow,
			Custom,
			FromControl
		}
	}

	public enum CaptionFormat
	{
		None,
		FieldName
	}
}
