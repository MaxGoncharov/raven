﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Raven.Commands;

namespace Raven.Controls
{
	public class ButtonWithIcon : Button
	{
		static ButtonWithIcon()
		{
			CommandProperty.OverrideMetadata(typeof(ButtonWithIcon), new FrameworkPropertyMetadata(CommandPropertyChangedCallback));
		}

		public static readonly DependencyProperty IconPathProperty = DependencyProperty.Register(nameof(IconPath), typeof(PathGeometry), typeof(ButtonWithIcon), new FrameworkPropertyMetadata(IconPathPropertyChangedCallback));
		public static readonly DependencyProperty WaitIconPathProperty = DependencyProperty.Register(nameof(WaitIconPath), typeof(PathGeometry), typeof(ButtonWithIcon), new FrameworkPropertyMetadata(WaitIconPathPropertyChangedCallback));
		public static readonly DependencyProperty CurrentIconPathProperty = DependencyProperty.Register(nameof(CurrentIconPath), typeof(PathGeometry), typeof(ButtonWithIcon));
		public static readonly DependencyProperty IsExecutingProperty = DependencyProperty.Register(nameof(IsExecuting), typeof(bool), typeof(ButtonWithIcon));

		private static void CommandPropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			var button = (ButtonWithIcon)obj;

			var oldCommand = args.OldValue as AsyncCommand;
			if (oldCommand != null)
			{
				oldCommand.PropertyChanged -= button.CommandOnPropertyChanged;
			}

			var newCommand = args.NewValue as AsyncCommand;
			if (newCommand != null)
			{
				newCommand.PropertyChanged += button.CommandOnPropertyChanged;
			}
		}

		private static void IconPathPropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			var button = (ButtonWithIcon)obj;
			button.CurrentIconPath = button.IsExecuting ? button.WaitIconPath : button.IconPath;
		}

		private static void WaitIconPathPropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			var button = (ButtonWithIcon)obj;
			button.CurrentIconPath = button.IsExecuting ? button.WaitIconPath : button.IconPath;
		}

		private void CommandOnPropertyChanged(object sender, PropertyChangedEventArgs args)
		{
			if (args.PropertyName == nameof(AsyncCommand.IsExecuting))
			{
				IsExecuting = ((AsyncCommand)sender).IsExecuting;
				CurrentIconPath = IsExecuting ? WaitIconPath : IconPath;
			}
		}

		public PathGeometry WaitIconPath
		{
			get { return (PathGeometry)GetValue(WaitIconPathProperty); }
			set { SetValue(WaitIconPathProperty, value); }
		}

		public PathGeometry IconPath
		{
			get { return (PathGeometry)GetValue(IconPathProperty); }
			set { SetValue(IconPathProperty, value); }
		}

		public PathGeometry CurrentIconPath
		{
			get { return (PathGeometry)GetValue(CurrentIconPathProperty); }
			private set { SetValue(CurrentIconPathProperty, value); }
		}

		public bool IsExecuting
		{
			get { return (bool)GetValue(IsExecutingProperty); }
			set { SetValue(IsExecutingProperty, value); }
		}
	}
}
