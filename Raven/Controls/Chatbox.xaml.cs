﻿using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Raven.Commands;

namespace Raven.Controls
{
	public partial class Chatbox : UserControl
	{
		public static readonly DependencyProperty ItemsSourceProperty = 
			DependencyProperty.Register(nameof(ItemsSource), typeof(IEnumerable), typeof(Chatbox), new FrameworkPropertyMetadata(null, ItemsSourceChanged));
		public static readonly DependencyProperty SendTextCommandProperty = 
			DependencyProperty.Register(nameof(SendTextCommand), typeof(ICommand), typeof(Chatbox), new FrameworkPropertyMetadata(null));

		private static void ItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			var control = (Chatbox)obj;
			var iNotifyPropertyChanged = args.OldValue as INotifyPropertyChanged;
			if (iNotifyPropertyChanged != null)
			{
				iNotifyPropertyChanged.PropertyChanged -= control.ItemsCollectionChanged;
			}
			iNotifyPropertyChanged = args.NewValue as INotifyPropertyChanged;
			if (iNotifyPropertyChanged != null)
			{
				iNotifyPropertyChanged.PropertyChanged += control.ItemsCollectionChanged;
			}
		}

		private void ItemsCollectionChanged(object obj, PropertyChangedEventArgs args)
		{
			ScrollViewer.ScrollToEnd();
		}

		public IEnumerable ItemsSource
		{
			get { return (IEnumerable)GetValue(ItemsSourceProperty); }
			set { SetValue(ItemsSourceProperty, value); }
		}

		public ICommand SendTextCommand
		{
			get { return (ICommand)GetValue(SendTextCommandProperty); }
			set { SetValue(SendTextCommandProperty, value); }
		}

		public Chatbox()
		{
			InitializeComponent();
			//ScrollViewer.ScrollChanged += ScrollViewerOnScrollChanged;
			tbMessage.TextChanged += (sender, args) => CommandManager.InvalidateRequerySuggested();
			bSend.Command = new RelayCommand(SendExecute, () => SendTextCommand?.CanExecute(GetText()) == true);
		}

		private void SendExecute()
		{
			SendTextCommand?.Execute(GetText());
			tbMessage.Text = string.Empty;
			Keyboard.Focus(tbMessage);
		}

		private string GetText()
		{
			return (tbMessage.Text ?? string.Empty).Trim(' ', '\n', '\r', '\t');
		}

		//private void ScrollViewerOnScrollChanged(object sender, ScrollChangedEventArgs scrollChangedEventArgs)
		//{
		//	var viewportHeight = ScrollViewer.ViewportHeight;
		//	for (int i = 0; i < ItemsControl.Items.Count; i++)
		//	{
		//		var control = ItemsControl.ItemContainerGenerator.ContainerFromIndex(i) as FrameworkElement;

		//		var p = control?.TransformToAncestor(ScrollViewer).Transform(new Point(0, control.ActualHeight / 2));
		//		if (p?.Y <= viewportHeight)
		//		{
		//			((TextMessageViewModel)ItemsControl.Items[i]).DeliveryStatus = DeliveryStatus.Viewed;
		//		}
		//	}
		//}

	}
}
