﻿using System.ComponentModel;
using System.Windows;
using Raven.Pages;

namespace Raven
{
	public partial class MainWindow : Window
	{
		private readonly RootPage _rootPage;
		public MainWindow()
		{
			InitializeComponent();
			_rootPage = new RootPage();
			Loaded += OnLoaded;
		}

		private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			ContentControl.Content = _rootPage;
			Loaded -= OnLoaded;
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			_rootPage.Dispose();
			base.OnClosing(e);
		}
	}
}
