﻿using System.ServiceModel;
using Raven.Core;

namespace Raven.Communication.Wcf
{
	[ServiceContract(CallbackContract = typeof(IChat))]
	internal interface IChat
	{
		[OperationContract(IsOneWay = true)]
		void Ping();

		[OperationContract(IsOneWay = false)]
		Client Connect(Client client);

		[OperationContract(IsOneWay = true)]
		void SendText(TextMessage message);

		[OperationContract(IsOneWay = true)]
		void Disconnect(string guid);
	}
}