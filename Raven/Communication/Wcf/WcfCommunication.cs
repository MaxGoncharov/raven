﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Threading.Tasks;
using Raven.Core;

namespace Raven.Communication.Wcf
{
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
	public class WcfCommunication : ITransport, IChat
	{
		private const int MaxConnection = 300;
		private TimeSpan OpenTimeout => new TimeSpan(0, 0, 0, 5);
		private TimeSpan CloseTimeout => new TimeSpan(0, 0, 0, 5);
		private TimeSpan SendTimeout => new TimeSpan(0, 0, 0, 5);
		private TimeSpan ReceiveTimeout => new TimeSpan(0, 5, 0);

		private readonly ChannelCollection _channelCollection = new ChannelCollection(MaxConnection);
		private Client _self;
		private ServiceHost _serviceHost;
		private bool _isDispose;

		public string Guid => "0280E410-FC52-4220-9DD8-94D24148884F";

		public event ReciveTextMessage ReciveTextMessageEvent;
		public event ChangedStatusTransport ChangedStatusTransportEvent;
		public event ClientConnected ClientConnectedEvent;
		public string ConnectionString => _self?.ConnectionString;

		public void SetMyTransportSettings(Client client)
		{
			_self = client;
		}

		public void Initialization()
		{
			Task.Run(async () =>
			{
				while (!_isDispose)
				{
					await Task.Delay(3000);
					var items = _channelCollection.GetSnapshotItems();
					Parallel.ForEach(items.Where(item => item.IsConnected), Ping);
				}
			});
			Task.Run(async () =>
			{
				while (!_isDispose)
				{
					var items = _channelCollection.GetSnapshotItems().Where(item => !item.IsConnected).ToArray();
					if (items.Length == 0)
					{
						await Task.Delay(3000);
					}
					else
					{
						Parallel.ForEach(items, Reconnect);
					}
				}
			});
		}

		private void Ping(ChannelCollection.Item item)
		{
			try
			{
				item.Chat.Ping();
			}
			catch (Exception)
			{
				var guid = item.Client?.Guid;
				if (guid != null)
				{
					ChangedStatusTransportEvent?.Invoke(this, guid, CommunicationStatus.Disconnected);
				}
				item.Close();
			}
		}

		private void Reconnect(ChannelCollection.Item item)
		{
			var guid = item.Client?.Guid;
			if (guid != null)
			{
				ChangedStatusTransportEvent?.Invoke(this, guid, CommunicationStatus.Connecting);
			}

			var context = new InstanceContext(this);
			var channelFactory = new DuplexChannelFactory<IChat>(context, CreateBinding(), new EndpointAddress(new Uri(item.ConnectionString)));
			try
			{
				IChat chat = item.Chat = channelFactory.CreateChannel();
				item.ChannelFactory = channelFactory;
				item.Client = chat.Connect(_self);
				guid = item.Client.Guid;
				if (!_channelCollection.Exist(item.ConnectionString) || _isDispose)
				{
					throw new Exception();
				}
				ClientConnectedEvent?.Invoke(this, item.Client);
			}
			catch (Exception)
			{
				item.Close();
				if (guid != null)
				{
					ChangedStatusTransportEvent?.Invoke(this, guid, CommunicationStatus.Disconnected);
				}
			}
		}

		public async Task Start(Client self)
		{
			await Task.Run(() =>
			{
				SetMyTransportSettings(self);
				IPAddress localIp = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
				Uri address = new Uri("net.tcp://" + localIp + ":30000");
				_self.ConnectionString = address.ToString();
				_serviceHost = new ServiceHost(this, address);

				var throttle = _serviceHost.Description.Behaviors.Find<ServiceThrottlingBehavior>();
				if (throttle == null)
				{
					throttle = new ServiceThrottlingBehavior();
					_serviceHost.Description.Behaviors.Add(throttle);
				}
				throttle.MaxConcurrentCalls = MaxConnection * 2;
				throttle.MaxConcurrentSessions = MaxConnection;
				throttle.MaxConcurrentInstances = throttle.MaxConcurrentCalls + throttle.MaxConcurrentSessions;

				_serviceHost.OpenTimeout = OpenTimeout;
				_serviceHost.CloseTimeout = CloseTimeout;

				_serviceHost.AddServiceEndpoint(typeof(IChat), CreateBinding(), address);

				try
				{
					_serviceHost.Open();
				}
				catch
				{
				}
			});
		}

		public void Stop()
		{
			try
			{
				_serviceHost.Close();
			}
			catch
			{
			}
			_channelCollection.Clear();
		}

		public void AddEndpoint(params string[] connectionString)
		{
			_channelCollection.Add(connectionString);
		}

		public void RemoveEndpoint(params string[] connectionStrings)
		{
			Task.Run(() => Parallel.ForEach(connectionStrings, s =>
			{
				var item = _channelCollection.FindByConnectionString(s);
				try
				{
					item.Chat?.Disconnect(_self.Guid);
				}
				catch
				{
				}
				var guid = item?.Client?.Guid;
				_channelCollection.Remove(item);
				if (guid != null)
				{
					ChangedStatusTransportEvent?.Invoke(this, guid, CommunicationStatus.Disconnected);
				}
			}));
		}

		public string[] GetEndpoints() => _channelCollection.GetSnapshotItems().Select(c => c.ConnectionString).ToArray();

		public async Task<bool> SendTextMessage(TextMessage message)
		{
			return await Task.Run(() =>
			{
				var channel = _channelCollection.FindByGuid(message.ToGuid);
				if (channel == null)
				{
					return false;
				}

				try
				{
					channel.Chat.SendText(message);
					return true;
				}
				catch (Exception)
				{
					ChangedStatusTransportEvent?.Invoke(this, channel.Client.Guid, CommunicationStatus.Error);
					channel.Close();
					return false;
				}
			});
		}

		public void Dispose()
		{
			_isDispose = true;
			Stop();
		}

		private Binding CreateBinding()
		{
			NetTcpBinding binding = new NetTcpBinding(SecurityMode.None, true);
			binding.TransferMode = TransferMode.Buffered;
			binding.MaxConnections = MaxConnection;
			binding.OpenTimeout = OpenTimeout;
			binding.SendTimeout = SendTimeout;
			binding.ReceiveTimeout = ReceiveTimeout;
			binding.ReliableSession.Enabled = true;
			binding.ReliableSession.InactivityTimeout = new TimeSpan(0, 1, 0);
			return binding;
		}

		void IChat.Ping()
		{
		}

		Client IChat.Connect(Client client)
		{
			var item = _channelCollection.FindByConnectionString(client.ConnectionString) ?? _channelCollection.FindByGuid(client.Guid);
			if (item == null)
			{
				item = _channelCollection.Add(client.ConnectionString);
				item.Client = client;
				item.Chat = OperationContext.Current.GetCallbackChannel<IChat>();
				ClientConnectedEvent?.Invoke(this, client);
			}
			else
			{
				item.Client = client;
				item.Chat = OperationContext.Current.GetCallbackChannel<IChat>();
				ChangedStatusTransportEvent?.Invoke(this, client.Guid, CommunicationStatus.Connected);
			}
			return _self;
		}

		void IChat.SendText(TextMessage message)
		{
			ReciveTextMessageEvent?.Invoke(this, message);
		}

		void IChat.Disconnect(string guid)
		{
			_channelCollection.FindByGuid(guid)?.Close();
			ChangedStatusTransportEvent?.Invoke(this, guid, CommunicationStatus.Disconnected);
		}
	}
}
