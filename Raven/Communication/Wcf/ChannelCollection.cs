﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Raven.Core;

namespace Raven.Communication.Wcf
{
	internal class ChannelCollection
	{
		public class Item
		{
			public Item(string connectionString)
			{
				ConnectionString = connectionString;
			}

			public string ConnectionString { get; }

			public Client Client { get; set; }

			public IChat Chat { get; set; }

			public ICommunicationObject ChannelFactory { get; set; }

			public bool IsConnected => Chat != null;
			
			public void Close()
			{
				var channelFactory = ChannelFactory;
				Chat = null;
				ChannelFactory = null;
				Client = null;
				channelFactory?.Abort();
			}
		}

		private readonly Item[] _items;

		public ChannelCollection(int maxConnection)
		{
			_items = new Item[maxConnection];
		}

		public Item[] GetSnapshotItems()
		{
			List<Item> list = new List<Item>(_items.Length);
			for (int i = 0; i < _items.Length; ++i)
			{
				var item = _items[i];
				if (item != null)
				{
					list.Add(item);
				}
			}
			return list.ToArray();
		}

		public Item FindByGuid(string guid) => FindItem(item => item?.Client?.Guid == guid);

		public Item FindByConnectionString(string connectionString) => FindItem(item => item?.ConnectionString == connectionString);

		public bool Exist(string connectionString) => FindByConnectionString(connectionString) != null;

		public Item Add(params string[] connectionStrings)
		{
			lock (_items)
			{
				foreach (var connectionString in connectionStrings)
				{
					int index = FindIndex(item => item?.ConnectionString == connectionString);
					if (index < 0)
					{
						index = FindIndex(item => item == null);
						if (index >= 0)
						{
							return _items[index] = new Item(connectionString);
						}
					}
				}
			}
			return null;
		}

		public void Remove(Item item)
		{
			if (item != null)
			{
				item.Close();
				int index = FindIndex(i => ReferenceEquals(i, item));
				if (index >= 0)
				{
					_items[index] = null;
				}
			}
		}

		public void Clear()
		{
			lock (_items)
			{
				for (int i = 0; i < _items.Length; ++i)
				{
					var item = _items[i];
					item?.Close();
					_items[i] = null;
				}
			}
		}

		private Item FindItem(Func<Item, bool> comparer)
		{
			for (int i = 0; i < _items.Length; ++i)
			{
				var item = _items[i];
				if (comparer(item))
				{
					return item;
				}
			}
			return null;
		}

		private int FindIndex(Func<Item, bool> comparer)
		{
			for (int i = 0; i < _items.Length; ++i)
			{
				var item = _items[i];
				if (comparer(item))
				{
					return i;
				}
			}
			return -1;
		}
	}
}