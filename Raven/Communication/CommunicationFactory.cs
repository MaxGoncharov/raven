﻿using Raven.Communication.Wcf;
using Raven.Core;

namespace Raven.Communication
{
	public static class CommunicationFactory
	{
		public static ITransport GetDefaultTransport => new WcfCommunication();
	}
}
