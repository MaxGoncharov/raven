﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Raven.Mvvm
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
	public class StringRangeLengthValidationAttribute : RangeAttribute
	{
		public StringRangeLengthValidationAttribute(int minLen, int maxLen) : base(minLen, maxLen)
		{
		}
		
		public override bool IsValid(object value)
		{
			try
			{
				var str = value as string ?? string.Empty;
				return (int)Minimum <= str.Length && str.Length <= (int)Maximum;
			}
			catch (OverflowException)
			{
				return false;
			}
		}

		public override string FormatErrorMessage(string name)
		{
			return string.IsNullOrEmpty(name) 
				? $"Длина текста должна быть в диапазоне: {Minimum}-{Maximum}." 
				: $"Длина текста в поле \"{name}\" должна быть в диапазоне: {Minimum}-{Maximum}.";
		}
	}
}
