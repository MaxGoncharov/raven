﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Raven.Annotations;

namespace Raven.Mvvm
{
	[Serializable]
	public abstract class ObservableObject : INotifyPropertyChanged, IDisposable
	{
		[NonSerialized]
		private PropertyChangedEventHandler _propertyChangedEvent;

		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				_propertyChangedEvent += value;
			}
			// ReSharper disable once DelegateSubtraction
			remove
			{
				_propertyChangedEvent -= value;
			}
		}

		public virtual void Dispose()
		{
		}

		[NotifyPropertyChangedInvocator]
		public virtual void OnPropertyChanged([CallerMemberName]string fieldName = null)
		{
			var propertyChangedEvent = _propertyChangedEvent;
			propertyChangedEvent?.Invoke(this, new PropertyChangedEventArgs(fieldName));
		}
	}
}
