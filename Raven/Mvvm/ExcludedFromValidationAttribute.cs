﻿using System;

namespace Raven.Mvvm
{
	[AttributeUsage(AttributeTargets.Property)]
	public class ExcludedFromValidationAttribute : Attribute
	{
	}
}