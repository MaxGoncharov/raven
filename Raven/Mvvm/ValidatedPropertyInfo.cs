﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Raven.Mvvm
{
	internal class ValidatedPropertyInfo : IValidatedObservableObjectItem
	{
		private readonly PropertyInfo _validatedProperty;
		private readonly ValidationAttribute[] _validators;
		private readonly ObservableObjectSupportedValidation _sourceObject;

		public ValidatedPropertyInfo(PropertyInfo validatedProperty, ObservableObjectSupportedValidation validatedObject)
		{
			if (validatedProperty == null)
			{
				throw new ArgumentNullException(nameof(validatedProperty));
			}

			_sourceObject = validatedObject;
			_validatedProperty = validatedProperty;
			_validators = GetValidationsAttributes(validatedProperty);
		}

		public string PropertyName => _validatedProperty.Name;

		public bool HasErrors => _validators.Any(x => !IsValid(x, GetPropertyValue()));

		private bool IsValid(ValidationAttribute validator, object value)
		{
			if (validator.RequiresValidationContext)
			{
				var result = validator.GetValidationResult(GetPropertyValue(), CreateValidationContext());
				return result == ValidationResult.Success;
			}
			return validator.IsValid(value);

		}

		public string ErrorString => !HasErrors ? string.Empty : BuildErrorString(string.Empty, string.Empty);

		public string FormattedErrorString => !HasErrors 
			? string.Empty 
			: BuildErrorString(ObservableObjectSupportedValidation.DefaultFormattedLinePattern, GetPropertyDisplayName());

		public static List<PropertyInfo> GetValidatedProperties(ObservableObjectSupportedValidation properties)
		{
			if (properties == null)
			{
				throw new ArgumentNullException(nameof(properties));
			}

			var propertiesWithValidators = properties.GetType()
				.GetProperties()
				.Where(p => GetValidationsAttributes(p).Length != 0 && !IsExcludedFromMainValidation(p))
				.ToList();

			return propertiesWithValidators;
		}

		public static ValidationAttribute[] GetValidationsAttributes(PropertyInfo property)
		{
			return (ValidationAttribute[])property.GetCustomAttributes(typeof(ValidationAttribute), true);
		}

		public static bool IsExcludedFromMainValidation(PropertyInfo property)
		{
			return property.GetCustomAttributes(typeof(ExcludedFromValidationAttribute), true).Any();
		}

		private string BuildErrorString(string linePattern, string propertyName)
		{
			var errorsCollection = new StringBuilder();
			var propertyValue = GetPropertyValue();
			foreach (var validator in _validators)
			{
				var errorText = BuildValidatorErrorString(validator, propertyName, propertyValue);
				if (string.IsNullOrEmpty(errorText))
				{
					continue;
				}

				if (!string.IsNullOrEmpty(linePattern))
				{
					errorsCollection.AppendFormat(linePattern, errorText);
				}
				else
				{
					errorsCollection.Append(errorText);
				}

				if (validator.Equals(_validators.Last()))
				{
					break;
				}

				errorsCollection.AppendLine();
			}

			return errorsCollection.ToString().Trim();
		}

		private string BuildValidatorErrorString(ValidationAttribute validator, string propertyName, object propertyValue)
		{
			if (validator.RequiresValidationContext)
			{
				var result = validator.GetValidationResult(propertyValue, CreateValidationContext());
				if (result == null || result == ValidationResult.Success)
				{
					return null;
				}
				return result.ErrorMessage;
			}

			if (validator.IsValid(propertyValue))
			{
				return null;
			}
			return string.IsNullOrEmpty(validator.ErrorMessage)
				? validator.FormatErrorMessage(propertyName)
				: validator.ErrorMessage;
		}

		private ValidationContext CreateValidationContext()
		{
			return new ValidationContext(_sourceObject)
			{
				MemberName = _validatedProperty.Name,
				DisplayName = GetPropertyDisplayName()
			};
		}

		private string GetPropertyDisplayName()
		{
			var attr = (DisplayNameAttribute)_validatedProperty.GetCustomAttribute(typeof(DisplayNameAttribute), true);
			return attr == null ? string.Empty : attr.DisplayName;
		}

		private object GetPropertyValue()
		{
			var getterValueFunction = GetValueGetter();
			return getterValueFunction(_sourceObject);
		}

		private Func<ObservableObjectSupportedValidation, object> GetValueGetter()
		{
			return viewmodel => _validatedProperty.GetValue(viewmodel, null);
		}
	}
}