﻿using System;
using System.Windows;

namespace Raven.Mvvm
{
	public class ModalWindowHelper
	{
		public static bool ShowDeleteWindow(int? count = 1, string additionalMessage = null)
		{
			count = count ?? 0;
			if (count == 0)
			{
				return false;
			}
			var message = count == 1 ? "Удалить выбраный элемент ?" : $"Удалить выбранные элементы ({count}) ?";
			if (!string.IsNullOrEmpty(additionalMessage))
			{
				message += Environment.NewLine;
				message += additionalMessage;
			}
			return ShowQuestionWindow(message, "Удаление");
		}

		public static bool ShowQuestionWindow(string message, string header = "Вопрос")
		{
			var result = MessageBox.Show(message, header, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
			return result == MessageBoxResult.Yes;
		}

		public static void ShowWarning(string message)
		{
			MessageBox.Show(message, "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);
		}

		public static void ShowError(string message)
		{
			MessageBox.Show(message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
		}

		public static bool ShowConformation(string message)
		{
			var r = MessageBox.Show(message, "Подтверждение", MessageBoxButton.YesNo, MessageBoxImage.Question);
			return r == MessageBoxResult.Yes;
		}

		public static void ShowInfornation(string message)
		{
			MessageBox.Show(message, "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
		}
	}
}