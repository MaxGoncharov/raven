﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Raven.Commands;

namespace Raven.Mvvm
{
	public abstract class ObservableObjectSupportedValidation : ObservableObject, IDataErrorInfo
	{
		private bool _validationEnabled = true;

		public const string DefaultFormattedLinePattern = "- {0}\r\n";

		protected ObservableObjectSupportedValidation()
		{
			var validatedProperties = ValidatedPropertyInfo.GetValidatedProperties(this);
			foreach (var property in validatedProperties)
			{
				var validatedPropInfo = new ValidatedPropertyInfo(property, this);
				ValidatedItems.Add(validatedPropInfo);
			}
		}

		public bool ValidationEnabled
		{
			get
			{
				return _validationEnabled;
			}
			set
			{
				_validationEnabled = value;
				OnPropertyChanged(string.Empty);
			}
		}

		public string this[string propertyName]
		{
			get
			{
				if (!ValidationEnabled)
				{
					return string.Empty;
				}
				var propertyInfo = ValidatedItems.TryGetProperty(propertyName);
				return propertyInfo != null ? propertyInfo.ErrorString : string.Empty;
			}
		}

		public virtual string Error
		{
			get
			{
				var errorBuilder = new StringBuilder();
				foreach (var property in ValidatedItems)
				{
					if (property.HasErrors)
					{
						errorBuilder.AppendLine(property.ErrorString);
					}
				}
				return errorBuilder.ToString().Trim();
			}
		}

		public virtual string FormattedError
		{
			get
			{
				var errorBuilder = new StringBuilder();
				foreach (var property in ValidatedItems)
				{
					if (property.HasErrors)
					{
						errorBuilder.AppendLine(property.FormattedErrorString);
					}
				}
				return errorBuilder.ToString().Trim();
			}
		}

		public virtual bool IsValid => !ValidationEnabled || ValidatedItems.All(x => !x.HasErrors);

		public virtual ICommand ApplyCommand => new RelayCommand(ApplyCommandExecute, ApplyCommandCanExecute);

		public virtual ICommand CancelCommand => new RelayCommand(CancelCommandExecute);

		protected virtual void ApplyCommandExecute()
		{
			if (!IsValid)
			{
				ShowErrorMessage();
			}
		}

		protected virtual bool ApplyCommandCanExecute() => true;

		protected virtual void CancelCommandExecute()
		{
		}

		protected List<IValidatedObservableObjectItem> ValidatedItems { get; } = new List<IValidatedObservableObjectItem>();

		protected virtual void ShowErrorMessage()
		{
			MessageBox.Show(FormattedError, "Ошибки", MessageBoxButton.OK, MessageBoxImage.Error);
		}
	}
}