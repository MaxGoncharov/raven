﻿namespace Raven.Mvvm
{
	public interface IValidatedObservableObjectItem
	{
		bool HasErrors { get; }
		string ErrorString { get; }
		string FormattedErrorString { get; }
	}
}
