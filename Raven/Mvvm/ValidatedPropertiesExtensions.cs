﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Raven.Mvvm
{
	internal static class ValidatedPropertiesExtensions
	{
		public static bool ContainsProperty(this IEnumerable<IValidatedObservableObjectItem> propertyCollection, string propertyName)
		{
			var list = propertyCollection.OfType<ValidatedPropertyInfo>();
			return list.Any(x => string.Equals(x.PropertyName, propertyName, StringComparison.Ordinal));
		}

		public static ValidatedPropertyInfo TryGetProperty(this List<IValidatedObservableObjectItem> propertiesCollection, string propertyName)
		{
			var list = propertiesCollection.OfType<ValidatedPropertyInfo>().ToList();
			return !list.ContainsProperty(propertyName) ? null : list.First(x => string.Equals(x.PropertyName, propertyName, StringComparison.Ordinal));
		}
	}
}