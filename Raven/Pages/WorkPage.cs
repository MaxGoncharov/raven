﻿using System.Threading.Tasks;
using Raven.Communication;
using Raven.Core;
using Raven.Pages.Data;
using Raven.Pages.Work;
using Raven.StateMachine;

namespace Raven.Pages
{
	public class WorkPage : NavigationRouter,
		INavigationPageInput<Storage>,
		INavigationPageOutput<RootNavigationDirection>
	{
		private ChatPage _chatPage;
		public async Task Initialization(Storage input)
		{
			_chatPage = new ChatPage(input, CommunicationFactory.GetDefaultTransport);
			RegistryStaticInstance(_chatPage);

			Bind<ChatPage, EditUserContactPage, WorkNavigationDirection, UserModel>(WorkNavigationDirection.EditUserContact);
			Bind<ChatPage, FindPage, WorkNavigationDirection, ITransport>(WorkNavigationDirection.Find);

			Bind<FindPage, ChatPage, WorkNavigationDirection>(WorkNavigationDirection.Chat);

			Bind<EditUserContactPage, ChatPage, WorkNavigationDirection, UserModel>(WorkNavigationDirection.Chat);

			End<ChatPage, WorkNavigationDirection>(WorkNavigationDirection.Goout, () =>
			{
				_chatPage.Save();
				_chatPage.Dispose();
				NavigatePageEvent?.Invoke(RootNavigationDirection.Loggin);
			});

			await Run<ChatPage>();
		}

		public event NavigatePageCallback<RootNavigationDirection> NavigatePageEvent;
	}
}