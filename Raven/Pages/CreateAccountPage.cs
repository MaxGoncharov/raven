﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Raven.Core;
using Raven.Mvvm;
using Raven.Pages.Data;
using Raven.StateMachine;

namespace Raven.Pages
{
	public class CreateAccountPage : ObservableObjectSupportedValidation,
		INavigationPageInput,
		INavigationPageOutput<RootNavigationDirection>
	{
		private string _username;
		private string _password;

		public Task Initialization() => Task.CompletedTask;

		public event NavigatePageCallback<RootNavigationDirection> NavigatePageEvent;

		[StringRangeLengthValidation(Storage.UsernameMinLenght, Storage.UsernameMaxLenght)]
		[DisplayName(@"Имя пользователя")]
		public string Username
		{
			get { return _username; }
			set
			{
				if (value == _username) return;
				_username = value;
				OnPropertyChanged();
			}
		}

		[StringRangeLengthValidation(Storage.PasswordMinLenght, Storage.PasswordMaxLenght)]
		[DisplayName(@"Пароль")]
		public string Password
		{
			get { return _password; }
			set
			{
				if (value == _password) return;
				_password = value;
				OnPropertyChanged();
			}
		}

		protected override async void ApplyCommandExecute()
		{
			base.ApplyCommandExecute();
			if (IsValid)
			{
				try
				{
					await Storage.Create(Properties.Settings.Default.UserdataDir, new Credential(Username, Password)).ConfigureAwait(false);
					ModalWindowHelper.ShowInfornation("Хранилище создано успешно!");
					NavigatePageEvent?.Invoke(RootNavigationDirection.Loggin);
				}
				catch (Exception exception)
				{
					ModalWindowHelper.ShowError(exception.Message);
				}
			}
		}

		protected override void CancelCommandExecute()
		{
			NavigatePageEvent?.Invoke(RootNavigationDirection.Loggin);
		}		
	}
}