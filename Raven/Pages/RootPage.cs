﻿using Raven.Core;
using Raven.Pages.Data;
using Raven.StateMachine;

namespace Raven.Pages
{
	public class RootPage : NavigationRouter
	{
		public RootPage()
		{
			Bind<LoginPage, CreateAccountPage, RootNavigationDirection>(RootNavigationDirection.CreateAccount);
			Bind<LoginPage, InitializePage, RootNavigationDirection, Credential>(RootNavigationDirection.FirstLoad);

			Bind<CreateAccountPage, LoginPage, RootNavigationDirection>(RootNavigationDirection.Loggin);

			Bind<InitializePage, LoginPage, RootNavigationDirection>(RootNavigationDirection.Loggin);
			Bind<InitializePage, WorkPage, RootNavigationDirection, Storage>(RootNavigationDirection.Work);

			Bind<WorkPage, LoginPage, RootNavigationDirection>(RootNavigationDirection.Loggin);

			Run<LoginPage>().Wait();
		}
	}
}
