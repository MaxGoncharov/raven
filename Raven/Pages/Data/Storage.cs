﻿using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Raven.Core;

namespace Raven.Pages.Data
{
	public class Storage
	{
		public const string FileExtension = ".rvnstg";

		public const int UsernameMinLenght = 5;
		public const int PasswordMinLenght = 5;

		public const int UsernameMaxLenght = 1024;
		public const int PasswordMaxLenght = 1024;

		const string FilenameInternal = "data";

		private readonly object _lockSave = new object();
		private Crypto _crypto;
		private UserModel[] _users = new UserModel[0];

		public string Filepath { get; private set; }

		public DateTimeOffset CreateTimeStamp { get; private set; }

		public UserModel[] Users
		{
			get { return _users; }
			set { _users = value ?? new UserModel[0]; }
		}

		public string UserGuid { get; private set; }

		public string DisplayName { get; set; }
		
		public static async Task Create(string directory, Credential credential)
		{
			Storage storage = new Storage();
			storage._crypto = new Crypto(credential.Password);
			storage.Filepath = Path.Combine(directory, GetFilename(credential.Username));
			storage.CreateTimeStamp = DateTimeOffset.Now;
			storage.UserGuid = Path.GetFileNameWithoutExtension(GetFilename(credential.Username));
			storage.DisplayName = credential.Username;
			await storage.Save().ConfigureAwait(false);
		}

		public void Load(string directory, Credential credential)
		{
			_crypto = new Crypto(credential.Password);
			Filepath = Path.Combine(directory, GetFilename(credential.Username));
			if (!File.Exists(Filepath))
			{
				throw new FileNotFoundException("Файл хранилища не найден." + Environment.NewLine + "Проверьте введенный логин или создайте новое хранилище (вкладка входа в систему -> новый)");
			}
			using (FileStream fileStream = new FileStream(Filepath, FileMode.Open))
			{
				using (ZipArchive archive = new ZipArchive(fileStream, ZipArchiveMode.Read))
				{
					ZipArchiveEntry entry = archive.GetEntry(FilenameInternal);
					if (entry == null)
					{
						throw new FileFormatException();
					}

					var model = JsonConvert.DeserializeObject<StorageModel>(_crypto.Decrypt(ReadAll(entry.Open())));
					CreateTimeStamp = model.CreateTimeStamp;
					Users = model.Users;
					UserGuid = model.UserGuid;
					DisplayName = model.DisplayName;
				}
			}
		}

		public async Task Save()
		{
			await Task.Run(() =>
			{
				lock (_lockSave)
				{
					var model = new StorageModel();
					model.CreateTimeStamp = CreateTimeStamp;
					model.Users = Users;
					model.UserGuid = UserGuid;
					model.DisplayName = DisplayName;
					var encryptedData = _crypto.Encrypt(JsonConvert.SerializeObject(model));

					File.Delete(Filepath);

					using (FileStream fileStream = new FileStream(Filepath, FileMode.OpenOrCreate))
					{
						using (ZipArchive archive = new ZipArchive(fileStream, ZipArchiveMode.Create))
						{
							ZipArchiveEntry entry = archive.CreateEntry(FilenameInternal);
							using (var writer = entry.Open())
							{
								writer.Write(encryptedData, 0, encryptedData.Length);
								writer.Flush();
							}
						}
					}
				}
			});
		}

		private static string GetFilename(string username)
		{
			using (HashAlgorithm sha = new SHA1CryptoServiceProvider())
			{
				var hashUsername = sha.ComputeHash(Encoding.UTF8.GetBytes(username));
				return Convert.ToBase64String(hashUsername, Base64FormattingOptions.None).Replace('+', '!').Replace('/', '-').TrimEnd('=') + FileExtension;
			}
		}

		public static byte[] ReadAll(Stream input, int maxBuff = 1024 * 1024 * 5)
		{
			var buffer = new byte[maxBuff];
			using (var memoryStream = new MemoryStream())
			{
				int read;
				while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
				{
					memoryStream.Write(buffer, 0, read);
				}
				return memoryStream.ToArray();
			}
		}
	}
}
