﻿using System;
using System.Runtime.Serialization;

namespace Raven.Pages.Data
{
	[DataContract]
	public class StorageModel
	{
		[DataMember]
		public DateTimeOffset CreateTimeStamp { get; set; }
		
		[DataMember]
		public UserModel[] Users { get; set; }

		[DataMember]
		public string UserGuid { get; set; }

		[DataMember]
		public string DisplayName { get; set; }
	}
}