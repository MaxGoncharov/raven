﻿using System;
using System.Runtime.Serialization;

namespace Raven.Pages.Data
{
	[DataContract]
	public class UserModel
	{
		public UserModel()
		{
		}

		public UserModel(string guid, string connectionString, string displayName, TextMessage[] messages = null)
		{
			Guid = guid;
			ConnectionString = connectionString;
			DisplayName = displayName;
			Messages = messages;
		}

		[DataMember]
		public string Guid { get; set; }

		[DataMember]
		public string DisplayName { get; set; }

		[DataMember]
		public string ConnectionString { get; set; }

		[DataMember]
		public string CryptKey { get; set; }

		[DataMember]
		public TextMessage[] Messages { get; set; }

		[DataContract]
		public class TextMessage
		{
			[DataMember]
			public DateTimeOffset Timestamp { get; set; }

			[DataMember]
			public string Text { get; set; }

			[DataMember]
			public bool Incoming { get; set; }
		}
	}
}