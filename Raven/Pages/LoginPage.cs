﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Raven.Commands;
using Raven.Core;
using Raven.Mvvm;
using Raven.Pages.Data;
using Raven.StateMachine;

namespace Raven.Pages
{
	public class LoginPage : ObservableObjectSupportedValidation,
		INavigationPageInput,
		INavigationPageOutput<RootNavigationDirection>,
		INavigationPageResultOutput<RootNavigationDirection, Credential>
	{
		private string _username;
		private string _password;

		public LoginPage()
		{
			CreateAccountCommand = new RelayCommand(() => NavigatePageEvent?.Invoke(RootNavigationDirection.CreateAccount));
		}

		public ICommand CreateAccountCommand { get; }

		[StringRangeLengthValidation(Storage.UsernameMinLenght, Storage.UsernameMaxLenght)]
		[DisplayName(@"Имя пользователя")]
		public string Username
		{
			get { return _username; }
			set
			{
				if (value == _username) return;
				_username = value;
				OnPropertyChanged();
			}
		}

		[StringRangeLengthValidation(Storage.PasswordMinLenght, Storage.PasswordMaxLenght)]
		[DisplayName(@"Пароль")]
		public string Password
		{
			get { return _password; }
			set
			{
				if (value == _password) return;
				_password = value;
				OnPropertyChanged();
			}
		}

		public Task Initialization() => Task.CompletedTask;

		protected override void ApplyCommandExecute()
		{
			base.ApplyCommandExecute();
			if (IsValid)
			{
				NavigatePageResultEvent?.Invoke(RootNavigationDirection.FirstLoad, new Credential(Username, Password));
			}
		}
		
		public event NavigatePageCallback<RootNavigationDirection> NavigatePageEvent;
		public event NavigatePageResultCallback<RootNavigationDirection, Credential> NavigatePageResultEvent;
	}
}