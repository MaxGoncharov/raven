﻿namespace Raven.Pages
{
	public enum WorkNavigationDirection
	{
		Chat,
		EditUserContact,
		DeleteContact,
		Goout,
		Find
	}
}