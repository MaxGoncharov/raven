﻿using System;

namespace Raven.Pages.Work
{
	public class TextMessageViewModel : MessageViewModel
	{
		public TextMessageViewModel(UserViewModel user, DateTimeOffset timestamp, string text) : base(user, timestamp)
		{
			Text = text;
		}

		public string Text { get; }
	}
}