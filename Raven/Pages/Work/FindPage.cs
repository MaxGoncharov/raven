﻿using System.ComponentModel;
using System.Threading.Tasks;
using Raven.Core;
using Raven.Mvvm;
using Raven.StateMachine;

namespace Raven.Pages.Work
{
	public class FindPage : ObservableObjectSupportedValidation,
		INavigationPageInput<ITransport>,
		INavigationPageOutput<WorkNavigationDirection>
	{
		private ITransport _transport;
		private string _connectionString;

		[DisplayName(@"Адрес")]
		[StringRangeLengthValidation(4, 255)]
		public string ConnectionString
		{
			get { return _connectionString; }
			set
			{
				if (value == _connectionString) return;
				_connectionString = value;
				OnPropertyChanged();
			}
		}

		protected override void ApplyCommandExecute()
		{
			base.ApplyCommandExecute();
			if (IsValid)
			{
				_transport.AddEndpoint(ConnectionString);
				NavigatePageEvent?.Invoke(WorkNavigationDirection.Chat);
			}
		}

		protected override void CancelCommandExecute()
		{
			NavigatePageEvent?.Invoke(WorkNavigationDirection.Chat);
		}

		public Task Initialization(ITransport transport)
		{
			_transport = transport;
			return Task.CompletedTask;
		}

		public event NavigatePageCallback<WorkNavigationDirection> NavigatePageEvent;
	}
}
