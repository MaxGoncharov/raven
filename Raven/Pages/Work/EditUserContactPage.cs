﻿using System.ComponentModel;
using System.Threading.Tasks;
using Raven.Mvvm;
using Raven.Pages.Data;
using Raven.StateMachine;

namespace Raven.Pages.Work
{
	public class EditUserContactPage : ObservableObjectSupportedValidation,
		INavigationPageInput<UserModel>,
		INavigationPageResultOutput<WorkNavigationDirection, UserModel>
	{
		private string _displayName;
		private string _connectionString;
		private string _guid;

		public Task Initialization(UserModel input)
		{
			Guid = input.Guid;
			DisplayName = input.DisplayName;
			ConnectionString = input.ConnectionString;
			return Task.CompletedTask;
		}

		public event NavigatePageResultCallback<WorkNavigationDirection, UserModel> NavigatePageResultEvent;

		public string Guid
		{
			get { return _guid; }
			private set
			{
				if (value == _guid) return;
				_guid = value;
				OnPropertyChanged();
			}
		}

		[DisplayName(@"Отображаемое имя")]
		[StringRangeLengthValidation(4, 1024)]
		public string DisplayName
		{
			get { return _displayName; }
			set
			{
				if (value == _displayName) return;
				_displayName = value;
				OnPropertyChanged();
			}
		}

		[DisplayName(@"Адрес")]
		[StringRangeLengthValidation(4, 255)]
		public string ConnectionString
		{
			get { return _connectionString; }
			set
			{
				if (value == _connectionString) return;
				_connectionString = value;
				OnPropertyChanged();
			}
		}

		protected override void ApplyCommandExecute()
		{
			base.ApplyCommandExecute();
			if (IsValid)
			{
				var guid = string.IsNullOrEmpty(Guid) ? System.Guid.NewGuid().ToString() : Guid;
				NavigatePageResultEvent?.Invoke(WorkNavigationDirection.Chat, new UserModel(guid, ConnectionString, DisplayName));
			}
		}

		protected override void CancelCommandExecute()
		{
			NavigatePageResultEvent?.Invoke(WorkNavigationDirection.Chat, null);
		}
	}
}
