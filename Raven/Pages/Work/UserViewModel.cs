﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Raven.Core;
using Raven.Mvvm;
using Raven.Pages.Data;

namespace Raven.Pages.Work
{
	public class UserViewModel : ObservableObject
	{
		private string _displayName;
		private bool _existUnreadMessage;
		private bool _isDisplayedNow;
		private CommunicationStatus _status = CommunicationStatus.Disconnected;

		private readonly ObservableCollection<MessageViewModel> _messageCollection =
			new ObservableCollection<MessageViewModel>();

		private string _connectionString;
		private ICollectionView _messageCollectionView;

		public UserViewModel(string guid, string connectionString, string displayName, UserModel.TextMessage[] messages = null)
		{
			Guid = guid;
			ConnectionString = connectionString;
			DisplayName = displayName;
			if (messages != null)
			{
				foreach (var message in messages)
				{
					_messageCollection.Add(new TextMessageViewModel(message.Incoming ? this : null, message.Timestamp, message.Text));
				}
			}
			Application.Current.Dispatcher.Invoke(() => { MessageCollectionView = new ListCollectionView(_messageCollection); });
		}

		public string Guid { get; }

		public string DisplayName
		{
			get { return _displayName; }
			set
			{
				if (value == _displayName) return;
				_displayName = value;
				OnPropertyChanged();
			}
		}

		public bool IsDisplayedNow
		{
			get { return _isDisplayedNow; }
			set
			{
				if (value == _isDisplayedNow) return;
				_isDisplayedNow = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(ExistUnreadMessage));
			}
		}

		public bool ExistUnreadMessage
		{
			get { return _existUnreadMessage; }
			protected set
			{
				if (value == _existUnreadMessage) return;
				_existUnreadMessage = value && !IsDisplayedNow;
				OnPropertyChanged();
			}
		}

		public string ConnectionString
		{
			get { return _connectionString; }
			set
			{
				if (value == _connectionString) return;
				_connectionString = value;
				OnPropertyChanged();
			}
		}

		public ICollectionView MessageCollectionView
		{
			get { return _messageCollectionView; }
			private set
			{
				if (Equals(value, _messageCollectionView)) return;
				_messageCollectionView = value;
				OnPropertyChanged();
			}
		}

		public CommunicationStatus Status
		{
			get { return _status; }
			set
			{
				if (value == _status) return;
				_status = value;
				OnPropertyChanged();
			}
		}

		public UserModel ToModel() => new UserModel(Guid, ConnectionString, DisplayName, GetMessages());

		public void PushMessage(TextMessage message)
		{
			Application.Current.Dispatcher.Invoke(() =>
			{
				ExistUnreadMessage = true;
				_messageCollection.Add(new TextMessageViewModel(Guid == message.ToGuid ? this : null, message.Timestamp, message.Text));
			});
		}

		public UserModel.TextMessage[] GetMessages()
		{
			return _messageCollection.OfType<TextMessageViewModel>()
					.Select(m => new UserModel.TextMessage() {Incoming = m.User != null, Text = m.Text, Timestamp = m.Timestamp})
					.ToArray();
		}
	}
}