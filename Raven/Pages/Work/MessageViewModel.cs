﻿using System;
using Raven.Mvvm;

namespace Raven.Pages.Work
{
	public abstract class MessageViewModel : ObservableObject
	{
		protected MessageViewModel(UserViewModel user, DateTimeOffset timestamp)
		{
			User = user;
			Timestamp = timestamp;
		}

		public UserViewModel User { get; }

		public DateTimeOffset Timestamp { get; }
	}
}