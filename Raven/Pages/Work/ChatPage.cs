﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Raven.Commands;
using Raven.Core;
using Raven.Mvvm;
using Raven.Pages.Data;
using Raven.StateMachine;

namespace Raven.Pages.Work
{
	public class ChatPage : ObservableObject,
		INavigationPageInput,
		INavigationPageInput<UserModel>,
		INavigationPageOutput<WorkNavigationDirection>,
		INavigationPageResultOutput<WorkNavigationDirection, UserModel>,
		INavigationPageResultOutput<WorkNavigationDirection, ITransport>
	{
		private readonly ObservableCollection<UserViewModel> _userCollection = new ObservableCollection<UserViewModel>();
		private UserViewModel _selectedUser;
		private readonly Storage _storage;
		private ListCollectionView _userCollectionView;
		private string _myAddress;
		private NavigatePageResultCallback<WorkNavigationDirection, ITransport> _navigatePageResultTransportCallback;

		public ChatPage(Storage storage, ITransport transport)
		{
			_storage = storage;
			Transport = transport;
			Guid = _storage.UserGuid;

			foreach (var item in storage.Users)
			{
				_userCollection.Add(new UserViewModel(item.Guid, item.ConnectionString, item.DisplayName, item.Messages));
			}

			Application.Current.Dispatcher.Invoke(() => UserCollectionView = new ListCollectionView(_userCollection));

			Transport.Initialization();
			Transport.ReciveTextMessageEvent += TransportOnReciveTextMessageEvent;
			Transport.ChangedStatusTransportEvent += TransportOnChangedStatusTransportEvent;
			Transport.ClientConnectedEvent += TransportOnClientConnectedEvent;
			Transport.Start(new Client() { DisplayName = storage.DisplayName, Guid = storage.UserGuid }).Wait();
			MyAddress = Transport.ConnectionString;
			Transport.AddEndpoint(storage.Users.Select(u => u.ConnectionString).ToArray());
			
			SendTextMessageCommand = new AsyncCommand<string>(SendTextMessageCommandExecute, SendTextMessageCommandCanExecute);
			DeleteUserCommand = new RelayCommand(DeleteUserCommandExecute, () => SelectedUser != null);
			EditUserCommand = new RelayCommand(() => NavigatePageResultEvent?.Invoke(WorkNavigationDirection.EditUserContact, SelectedUser.ToModel()), () => SelectedUser != null);
			AddUserCommand = new RelayCommand(() => _navigatePageResultTransportCallback?.Invoke(WorkNavigationDirection.Find, Transport));
			ExitCommand = new RelayCommand(() => NavigatePageEvent?.Invoke(WorkNavigationDirection.Goout));
		}

		public event NavigatePageCallback<WorkNavigationDirection> NavigatePageEvent;
		public event NavigatePageResultCallback<WorkNavigationDirection, UserModel> NavigatePageResultEvent;
		event NavigatePageResultCallback<WorkNavigationDirection, ITransport> INavigationPageResultOutput<WorkNavigationDirection, ITransport>.NavigatePageResultEvent
		{
			add { _navigatePageResultTransportCallback += value; }
			remove { _navigatePageResultTransportCallback -= value; }
		}

		public ListCollectionView UserCollectionView
		{
			get { return _userCollectionView; }
			private set
			{
				if (Equals(value, _userCollectionView)) return;
				_userCollectionView = value;
				OnPropertyChanged();
			}
		}

		public string Guid { get; }
		
		public ICommand AddUserCommand { get; }
		public ICommand EditUserCommand { get; }
		public ICommand DeleteUserCommand { get; }
		public ICommand SendTextMessageCommand { get; }
		public ICommand ExitCommand { get; }

		public ITransport Transport { get; }

		public string MyAddress
		{
			get { return _myAddress; }
			private set
			{
				if (value == _myAddress) return;
				_myAddress = value;
				OnPropertyChanged();
			}
		}

		public UserViewModel SelectedUser
		{
			get { return _selectedUser; }
			set
			{
				if (Equals(value, _selectedUser)) return;
				if (_selectedUser != null)
				{
					_selectedUser.IsDisplayedNow = false;
				}
				_selectedUser = value;
				if (_selectedUser != null)
				{
					_selectedUser.IsDisplayedNow = true;
				}
				OnPropertyChanged();
				CommandManager.InvalidateRequerySuggested();
			}
		}

		public Task Initialization() => Task.CompletedTask;

		public Task Initialization(UserModel input)
		{
			if (input != null)
			{
				Application.Current.Dispatcher.Invoke(() =>
				{
					var contract = _userCollection.FirstOrDefault(c => c.Guid == input.Guid);
					if (contract == null)
					{
						_userCollection.Add(new UserViewModel(input.Guid, input.ConnectionString, input.DisplayName));
					}
					else
					{
						Transport.RemoveEndpoint(contract.ConnectionString);
						contract.DisplayName = input.DisplayName;
						contract.ConnectionString = input.ConnectionString;
					}
					Transport.AddEndpoint(input.ConnectionString);
					Save();
				});
			}
			return Task.CompletedTask;
		}

		public override void Dispose()
		{
			base.Dispose();
			Transport.Dispose();
		}

		public void Save()
		{
			Task.Run(() =>
			{
				lock (_storage)
				{
					_storage.Users = _userCollection.Select(u => u.ToModel()).ToArray();
					_storage.Save().Wait();
				}
			});
		}

		private async Task SendTextMessageCommandExecute(string text)
		{
			text = text?.Trim(' ', '\r', '\n');
			var contact = SelectedUser;
			if (contact != null && !string.IsNullOrEmpty(text))
			{
				var message = new TextMessage()
				{
					Number = 0,
					Timestamp = DateTimeOffset.Now,
					Text = text,
					FromGuid = Guid,
					ToGuid = contact.Guid
				};
				await Transport.SendTextMessage(message).ConfigureAwait(false);
				SelectedUser.PushMessage(message);
			}
		}

		private bool SendTextMessageCommandCanExecute(string s) => SelectedUser?.Status == CommunicationStatus.Connected;

		private void DeleteUserCommandExecute()
		{
			if (ModalWindowHelper.ShowDeleteWindow())
			{
				Transport.RemoveEndpoint(SelectedUser.ConnectionString);
				_userCollection.Remove(SelectedUser);
				Save();
			}
		}

		private void TransportOnReciveTextMessageEvent(ITransport transport, TextMessage message)
		{
			Application.Current.Dispatcher.Invoke(() =>
			{
				var user = _userCollection.FirstOrDefault(u => u.Guid == message.FromGuid);
				user?.PushMessage(message);
			});
		}

		private void TransportOnChangedStatusTransportEvent(ITransport transport, string guid, CommunicationStatus status)
		{
			Application.Current.Dispatcher.Invoke(() =>
			{
				var user = _userCollection.FirstOrDefault(u => u.Guid == guid);
				if (user != null)
				{
					user.Status = status;
				}
			});
		}

		private void TransportOnClientConnectedEvent(ITransport transport, Client client)
		{
			Application.Current.Dispatcher.Invoke(() =>
			{
				var user = _userCollection.FirstOrDefault(u => u.Guid == client.Guid);
				if (user == null)
				{
					user = new UserViewModel(client.Guid, client.ConnectionString, client.DisplayName);
					_userCollection.Add(user);
				}
				user.Status = CommunicationStatus.Connected;
			});
		}
	}
}