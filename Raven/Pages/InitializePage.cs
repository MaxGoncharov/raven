﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Raven.Commands;
using Raven.Core;
using Raven.Mvvm;
using Raven.Pages.Data;
using Raven.StateMachine;

namespace Raven.Pages
{
	public class InitializePage : ObservableObject,
		INavigationPageInput<Credential>,
		INavigationPageOutput<RootNavigationDirection>,
		INavigationPageResultOutput<RootNavigationDirection, Storage>
	{
		private string _textError;

		public InitializePage()
		{
			BackCommand = new RelayCommand(() => NavigatePageEvent?.Invoke(RootNavigationDirection.Loggin));
		}

		public async Task Initialization(Credential input)
		{
			Storage storage = new Storage();
			try
			{
				await Task.Run(() => storage.Load(Properties.Settings.Default.UserdataDir, input)).ConfigureAwait(false);
				NavigatePageResultEvent?.Invoke(RootNavigationDirection.Work, storage);
			}
			catch (Exception exception)
			{
				TextError = exception.Message;
			}
		}

		public ICommand BackCommand { get; }

		public bool HasError => !string.IsNullOrEmpty(TextError);

		public string TextError
		{
			get { return _textError; }
			private set
			{
				if (value == _textError) return;
				_textError = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(HasError));
			}
		}

		public event NavigatePageCallback<RootNavigationDirection> NavigatePageEvent;
		public event NavigatePageResultCallback<RootNavigationDirection, Storage> NavigatePageResultEvent;
	}
}
